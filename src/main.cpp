#include <Arduino.h>
#include "AWSParameters.h"
#include "IOTGateway.h"
#include "StandardConfig.h"
#include "U8x8lib.h"
#include "Sensor.h"
#include <NTPClient.h>
#include <WiFiUdp.h>

// Anything below this is pretty much immersion in tap water
#define PURE_WATER 1465
#define DRY 4095
#define PIN 34

IOTGateway iotGateway(&capgeminiHackathon);
U8X8_SSD1306_128X64_NONAME_SW_I2C display(4, 5, U8X8_PIN_NONE);
Sensor sensor("moisture", "moisture", 5000, 5000);

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 0, 60000);

void setup() {
  Serial.begin(115200);

  display.begin();
  display.clear();
  
  display.setFont(u8x8_font_amstrad_cpc_extended_f);
  display.drawString(0,0,"Start...");
  display.refreshDisplay();

  iotGateway.initialise();
  timeClient.begin();
  timeClient.update();

  delay(2500);
  
  display.clear();
  display.drawString(0,0,"Moisture:");
  display.refreshDisplay();
  display.setFont(u8x8_font_courB18_2x3_f);

  pinMode(PIN, INPUT);
}

char msg[512];
char scrn_msg[5];
char other_msg[5];
char ts_txt[16];
long lastTime = 0;

void loop() {
  int16_t value = analogRead(PIN);
  int waterAmount = max(0, min(100, (DRY - value) * 100 / (DRY - PURE_WATER)));

  if (millis() - lastTime > 30000) {
    char fullTimestamp [25];
    sprintf (fullTimestamp, "%ld", timeClient.getEpochTime());
    lastTime = millis();
    sensor.formatData(msg, 512, waterAmount, "moisture", fullTimestamp);

    iotGateway.publish("measurement/moisture", msg);
  }

  sprintf(scrn_msg, "%03d%%", waterAmount);
  display.drawString(1, 1, scrn_msg);
  display.refreshDisplay();

  delay(1000);
}